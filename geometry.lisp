(in-package #:crawler)

(defun room-to-coords (room)
  (list :top-left (make-instance 'coord :row (getf room :row) :col (getf room :col))
        :top-right (make-instance 'coord :row (getf room :row) :col (+ (getf room :col) (getf room :width)))
	:bottom-left (make-instance 'coord :row (+ (getf room :row) (getf room :height)) :col (getf room :col))
	:bottom-right (make-instance 'coord :row (+ (getf room :row) (getf room :height)) :col (+ (getf room :col) (getf room :width)))
	))

(defun gen-coords-along-line (starting-point ending-point)
  (let ((line nil))
    (loop for row from (min (row starting-point) (row ending-point)) to (max (row starting-point) (row ending-point))
       do (loop for col from (min (col starting-point) (col ending-point)) to (max (col starting-point) (col ending-point))
	     do (push (make-instance 'coord :row row :col col) line)))
    (reverse line)))

(defun point-in-square (point square-coords)
  (and (>= (row point) (row (getf square-coords :top-left)))
       (<= (row point) (row (getf square-coords :bottom-left)))
       (>= (col point) (col (getf square-coords :top-left)))
       (<= (col point) (col (getf square-coords :top-right)))))

(defun print-list-out (plist)
  (for:for ((i over (plist-to-list plist)))
    (format t "row: ~a col: ~a~%" (row i) (col i))))


