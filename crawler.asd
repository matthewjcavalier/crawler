;;;; crawler.asd

(asdf:defsystem #:crawler
  :description "Describe crawler here"
  :author "Matt Cavalier <matt@cavalier.dev>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:for
		  #:random-state)
  :components ((:file "package")
	       (:file "symbols")
	       (:file "objects")
	       (:file "util")
	       (:file "geometry")
	       (:file "draw")
	       (:file "room-gen")
	       (:file "floor-gen")
               (:file "crawler")))
