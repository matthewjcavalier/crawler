;;;; package.lisp

(defpackage #:crawler
  (:use #:cl)
  (:export #:point-in-square
	   #:plist-to-list
	   #:coord
	   #:row
	   #:col
	   #:room-to-coords
	   #:are-coords-equal
	   #:is-valid-room
	   #:gen-basic-settings
	   #:gen-coords-along-line))
