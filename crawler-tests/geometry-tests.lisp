(in-package #:crawler-tests)

(defun gen-square-points ()
  (list :top-left     (make-instance 'coord :row 1 :col 1)
	:top-right    (make-instance 'coord :row 1 :col 5)
	:bottom-left  (make-instance 'coord :row 5 :col 1)
	:bottom-right (make-instance 'coord :row 5 :col 5)))

(defun make-point (row col)
  (make-instance 'coord :row row :col col))

(deftest point-in-square-true
  (testing "(3,3) is in square (1,1) (1,5) (5,1) (5,5)"
    (let ((square-coords (gen-square-points))
	  (point (make-point 3 3)))
      (ok (crawler:point-in-square point square-coords)))))

(deftest point-in-square-true-point-on-left-border
  (testing "(3,1) is in square (1,1) (1,5) (5,1) (5,5)"
    (let ((square-coords (gen-square-points))
	  (point (make-point 3 1)))
      (ok (crawler:point-in-square point square-coords)))))

(deftest point-in-square-true-point-on-right-border
  (testing "(3,1) is in square (1,1) (1,5) (5,1) (5,5)"
    (let ((square-coords (gen-square-points))
	  (point (make-point 3 5)))
      (ok (crawler:point-in-square point square-coords)))))

(deftest point-in-square-true-point-on-top-border
  (testing "(3,1) is in square (1,1) (1,5) (5,1) (5,5)"
    (let ((square-coords (gen-square-points))
	  (point (make-point 1 3)))
      (ok (crawler:point-in-square point square-coords)))))

(deftest point-in-square-true-point-on-bottom-border
  (testing "(3,1) is in square (1,1) (1,5) (5,1) (5,5)"
    (let ((square-coords (gen-square-points))
	  (point (make-point 5 3)))
      (ok (crawler:point-in-square point square-coords)))))

(deftest point-in-square-true-point-on-corners
  (testing "All corners are in square (1,1) (1,5) (5,1) (5,5)"
    (let ((square-coords (gen-square-points)))
      (for:for ((point over (crawler:plist-to-list square-coords)))
	(ok (crawler:point-in-square point square-coords)
	    (format nil "(r:~a, c:~a) should be in square" (slot-value point 'crawler::row) (slot-value point 'crawler::col)))))))


(deftest room-to-cords-test
  (testing "room is properly converted to coordinates"
    (let ((the-room (list :row 5 :col 5 :height 5 :width 5))
	  (top-left (make-point 5 5))
	  (top-right (make-point 5 10))
	  (bottom-left (make-point 10 5))
	  (bottom-right (make-point 10 10)))
      (let ((coords (crawler:room-to-coords the-room)))
	(ok (crawler:are-coords-equal top-left (getf coords :top-left)))
	(ok (crawler:are-coords-equal top-right (getf coords :top-right)))
	(ok (crawler:are-coords-equal bottom-left (getf coords :bottom-left)))
	(ok (crawler:are-coords-equal bottom-right (getf coords :bottom-right)))))))

(deftest gen-coords-along-line-test-vert
  (testing "giving two points that form a vertical line, a series of points are returned that make up the vertical line"
    (let* ((top-coord (make-instance 'coord :row 0 :col 0))
	   (bottom-coord (make-instance 'coord :row 3 :col 0))
	   (the-line (crawler:gen-coords-along-line top-coord bottom-coord))
	   (expected (let ((points NIL))
		       (loop for i from 0 to 3
			  do (push (make-instance 'coord :row i :col 0) points))
		       points)))
      (for:for ((point over expected))
	(ok (member point the-line :test #'crawler:are-coords-equal))))))

(deftest gen-coords-along-line-test-vert-reversed
  (testing "giving two points that form a vertical line, a series of points are returned that make up the vertical line"
    (let* ((top-coord (make-instance 'coord :row 0 :col 0))
	   (bottom-coord (make-instance 'coord :row 3 :col 0))
	   (the-line (crawler:gen-coords-along-line bottom-coord top-coord))
	   (expected (let ((points NIL))
		       (loop for i from 0 to 3
			  do (push (make-instance 'coord :row i :col 0) points))
		       points)))
      (for:for ((point over expected))
	(ok (member point the-line :test #'crawler:are-coords-equal))))))

(deftest gen-coords-along-line-test-horiz
  (testing "giving two points that form a vertical line, a series of points are returned that make up the vertical line"
    (let* ((top-coord (make-instance 'coord :row 0 :col 0))
	   (bottom-coord (make-instance 'coord :row 0 :col 3))
	   (the-line (crawler:gen-coords-along-line top-coord bottom-coord))
	   (expected (let ((points NIL))
		       (loop for i from 0 to 3
			  do (push (make-instance 'coord :row 0 :col i) points))
		       points)))
      (for:for ((point over expected))
	(ok (member point the-line :test #'crawler:are-coords-equal))))))

(deftest gen-coords-along-line-test-horiz-reversed
  (testing "giving two points that form a vertical line, a series of points are returned that make up the vertical line"
    (let* ((top-coord (make-instance 'coord :row 0 :col 0))
	   (bottom-coord (make-instance 'coord :row 0 :col 3))
	   (the-line (crawler:gen-coords-along-line bottom-coord top-coord))
	   (expected (let ((points NIL))
		       (loop for i from 0 to 3
			  do (push (make-instance 'coord :row 0 :col i) points))
		       points)))
      (for:for ((point over expected))
	(ok (member point the-line :test #'crawler:are-coords-equal))))))


