;;;; util-tests.lisp

(in-package #:crawler-tests)

;;;;(defvar (crawler:print-list-out (list :a 1 :b 2)))

(deftest plist-to-list-test ()
	 (testing "plist of (:a 1 :b 2 :c 3 :d 4) -> (1 2 3 4)"
	   (ok (equal (list 1 2 3 4) (crawler:plist-to-list (list :a 1 :b 2 :c 3 :d 4))))))
