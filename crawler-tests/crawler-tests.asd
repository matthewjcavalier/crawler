;;;; crawler-tests.asd

;;;; to run these tests: (rove:run :crawler-tests)

(asdf:defsystem #:crawler-tests
  :description "Describe crawler-tests here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:crawler
	       #:rove
	       #:for)
  :components ((:file "package")
               (:file "crawler-tests")
	       (:file "util-tests")
	       (:file "geometry-tests")
	       (:file "room-gen-tests")))
