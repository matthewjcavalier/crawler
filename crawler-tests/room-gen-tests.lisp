(in-package #:crawler-tests)

(defvar *basic-room* NIL)
(defvar *standard-settings* NIL)
(defvar *large-room* NIL)

(defhook :before
  (setq *basic-room* (list :row 0 :col 0 :height 3 :width 3))
  (setq *large-room* (list :row 10 :col 10 :height 10 :width 10))
  (setq *standard-settings*
	(let ((base-settings (crawler:gen-basic-settings)))
	  (progn
	    (setf (getf base-settings :max-row) 100)
	    (setf (getf base-settings :max-col) 100)
	    base-settings))))



;;;;...
;;;;...
;;;;...
;;;;
;;;;
;;;;     ...
;;;;     ...
;;;;     ...
;;;;

(deftest valid-room
  (testing "No room overlap, within bounds"
    (let ((rooms-list (list *basic-room*))
	  (room (list :row 5 :col 5 :height 3 :width 3)))
      (ok (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-OOB-vert-less-than-0
  (testing "When room has row of -1 the room should be invalid"
    (let ((rooms-list nil)
	  (room (list :row -1 :col 5 :height 3 :width 3)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-OOB-horizontal-less-than-0
  (testing "When room has col of -1 the room should be invalid"
    (let ((rooms-list nil)
	  (room (list :row 5 :col -1 :height 3 :width 3)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-OOB-vert-and-horizontal-less-than-0
  (testing "When room has row and col of -1 the room should be invalid"
    (let ((rooms-list nil)
	  (room (list :row -1 :col -1 :height 3 :width 3)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

;;;;
;;;;  _____
;;;; |  ______ 
;;;; | |  |   |
;;;; | |______|
;;;; |____|     


(deftest invalid-room-right-side-overlap
  (testing "When room has upper left and lower left corners in another room and other corners outside of room that room should be invalid"
    (let ((rooms-list (list *large-room*))
	  (room (list :row 15 :col 15 :height 3 :width 10)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

;;;;
;;;;
;;;;
;;;;    ........
;;;;    ........
;;;;  __........
;;;; |  ........
;;;; |  ........
;;;; |    |
;;;; |____|


(deftest invalid-room-top-right-corner-overlap
  (testing "When room bottom left corner in another room, and all other corners outside the room, then the new room is invalid"
    (let ((rooms-list (list *large-room*))
	  (room (list :row 5 :col 15 :height 10 :width 10)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-bottom-right-corner-overlap
  (testing "When room top right corner in another room, and all other corners outside the room, then the new room is invalid"
    (let ((rooms-list (list *large-room*))
	  (room (list :row 15 :col 15 :height 10 :width 10)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-bottom-left-corner-overlap
  (testing "When room top left corner in another room, and all other corners outside the room, then the new room is invalid"
    (let ((rooms-list (list *large-room*))
	  (room (list :row 15 :col 15 :height 10 :width 10)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-right-side-overlap
  (testing "When room top right corner and bottom right corner in another room, and all other corners outside the room, then the new room is invalid"
    (let ((rooms-list (list *large-room*))
	  (room (list :row 15 :col 5 :height 3 :width 10)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-bottom-side-overlap
  (testing "When room bottom right corner and bottom left corner in another room, and all other corners outside the room, then the new room is invalid"
    (let ((rooms-list (list *large-room*))
	  (room (list :row 5 :col 15 :height 10 :width 3)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-top-side-overlap
  (testing "When room top right corner and top left corner in another room, and all other corners outside the room, then the new room is invalid"
    (let ((rooms-list (list *large-room*))
	  (room (list :row 15 :col 15 :height 10 :width 3)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-inside-other-room
  (testing "When a room is completely inside another room"
    (let ((rooms-list (list *large-room*)))
      (ng (crawler:is-valid-room *large-room* rooms-list *standard-settings*)))))

(deftest invalid-room-next-to-room-no-buffer
  (testing "When a room is next another room with no one column buffer"
    (let ((rooms-list (list *large-room*))
	  (room (list :row 10 :col 20 :height 10 :width 10)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-above-room-no-buffer
  (testing "When a room is above another room with no one row buffer"
    (let ((rooms-list (list *large-room*))
	  (room (list :row 5 :col 10 :height 5 :width 10)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))

(deftest invalid-room-plus-symbol
  (testing "When a room overlaps another room in such a way that they make a plus symbol, and none of their corners are in each other"
    (let ((rooms-list (list (list :row 16 :col 67 :width 13 :height 5)))
	  (room (list :row 14 :col 70 :height 10 :width 8)))
      (ng (crawler:is-valid-room room rooms-list *standard-settings*)))))
