;;;; package.lisp

(defpackage #:crawler-tests
  (:use #:cl)
  (:shadowing-import-from #:rove
			  #:deftest
			  #:testing
			  #:ng
			  #:ok
			  #:defhook)
  (:shadowing-import-from #:crawler
			  #:coord
			  #:point-in-square)
  (:export #:invalid-room-plus-symbol))
