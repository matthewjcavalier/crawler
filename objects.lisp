(in-package #:crawler)

(defclass coord ()
  ((row
    :initarg :row
    :accessor row)
   (col
    :initarg :col
    :accessor col)))

(defun are-coords-equal (c1 c2)
  (and (= (row c1) (row c2)) (= (col c1) (col c2))))

(defun describe-coord (c)
  (format t "(r: ~a, c: ~a)~%" (row c) (col c)))

(defclass tile ()
  ((hardness
    :initarg :hardness
    :accessor hardness)
   (tile-type
    :initarg :tile-type
    :accessor tile-type)))

(defmethod clone ((obj tile))
  (make-instance 'tile :hardness (hardness obj) :tile-type (tile-type obj)))

(defmethod describe-tile ((obj tile))
  (format T "Hardness: ~a~10T Type: ~a~%" (hardness obj) (tile-type obj)))


