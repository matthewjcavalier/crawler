(in-package #:crawler)

(defun gen-basic-settings ()
  (gen-settings :max-row 40
		:max-col 160
		:max-stone-hardness 10
		:min-num-rooms 5))

(defun gen-settings (&key max-row max-col min-num-rooms max-stone-hardness)
  (list :max-row max-row
	:max-col max-col
	:min-num-rooms min-num-rooms
	:max-stone-hardness max-stone-hardness
	:min-room-height 3
	:min-room-width 4))

(defun generate-dungeon ()
  (let ((settings (gen-basic-settings)))
    ;; Genreate a random dungeon
    (let ((dun NIL)
	  (rooms (generate-all-rooms settings))
	  (dun-frame (make-border (gen-grid
				   (getf settings :max-row)
				   (getf settings :max-col)
				   (getf settings :max-stone-hardness)))))
      (progn
	(setq dun (add-rooms-to-dun rooms dun-frame))
	(let ((halls (gen-hallways rooms)))
	  (add-halls-to-dun halls dun))
	dun))))

(defun gen-grid (height width max-val)
  ;; Generates a 2d grid of height x width that contains random numbers from 1 to max-val
  (let ((game-map NIL) (map-row NIL))
    (dotimes (row height)
      (progn
	(dotimes (col width)
	  (push (make-instance 'tile :hardness (get-rand-int max-val) :tile-type 'wall) map-row))
	(push map-row game-map)
	(setq map-row NIL)))
    game-map))

(defun make-border (arr)
  (dotimes (row  (length arr))
    (dotimes (col (length (nth 0 arr)))
      (if (or (= 0 row) (= (- (length arr) 1) row) (= 0 col) (= (- (length (nth 0 arr)) 1) col))
	  (progn (setf (hardness (nth col (nth row arr))) 1000)
		 (setf (tile-type (nth col (nth row arr))) 'border)))))
  arr)

(defun add-rooms-to-dun (rooms dun-grid)
  (for:for ((room over rooms))
    (add-room-to-dun room dun-grid))
  dun-grid)

(defun add-room-to-dun (room dun-grid)
  (loop
     for row from (getf room :row) to (+ (getf room :row) (getf room :height))
     do (loop for col from (getf room :col) to (+ (getf room :col) (getf room :width))
	   do (progn (setf (hardness (nth col (nth row dun-grid))) 0)
		     (setf (tile-type (nth col (nth row dun-grid))) 'room)))))

(defun add-halls-to-dun (halls dun)
  (for:for ((point over halls))
    (if (not (eq (tile-type (nth (col point) (nth (row point) dun))) 'wall))
	(setf (hardness (nth (col point) (nth (row point) dun))) 0)
	(setf (tile-type (nth (col point) (nth (row point) dun))) 'hall))))


(defun gen-hallways (rooms)
  (let* ((curr-room (pop rooms))
	 (next-room (pop rooms))
	 (all-paths NIL))
    (loop while rooms
       do (progn
	    (setq all-paths (append all-paths (make-hall-path curr-room next-room)))
	    (setq curr-room next-room)
	    (setq next-room (pop rooms))))
    all-paths))


(defun make-hall-path (p1 p2)
  (let ((path-made NIL)
	(path NIL)
	(curr-row (getf p1 :row))
	(curr-col (getf p1 :col))
	(p2-row (getf p2 :row))
	(p2-col (getf p2 :col))
	(p2-point (make-instance 'coord :row (getf p2 :row) :col (getf p2 :col))))
    (loop while (not path-made)
       do (let ((next-point (gen-next-path-point curr-row curr-col p2-row p2-col)))
	    (progn
	      (setq curr-row (row next-point))
	      (setq curr-col (col next-point))
	      (push next-point path)
	      (setq path-made (are-coords-equal next-point p2-point)))))
    path))

(defun gen-next-path-point (curr-row curr-col target-row target-col)
  (let ((decider (get-rand-int 1))
	(new-row curr-row)
	(new-col curr-col)
	(hoizontal-match (= curr-col target-col))
	(vertical-match (= curr-row target-row)))
    (cond ((and (not hoizontal-match) (= 0 decider)) ;; do horizontal move set
	   (setq new-col (+ curr-col  (if (> (- target-col curr-col) 0) 1 -1))))
	  ((not vertical-match)
	   (setq new-row (+ curr-row (if (> (- target-row curr-row) 0) 1 -1)))))
    (make-instance 'coord :row new-row :col new-col)))

