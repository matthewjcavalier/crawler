(in-package #:crawler)

(defun plist-to-list (plist)
  (let ((new-list nil) (counter 1))
    (for:for ((item over plist))
      (if (= (mod counter 2) 0) (push item new-list)) (setq counter (+ counter 1)))
    (reverse new-list)))

(defun describe-rooms (rooms)
  (for:for ((room over rooms))
    (describe-room room)))

(defun describe-room (room)
  (format t "R: ~a C: ~a H: ~a W: ~a~%" (getf room :row) (getf room :col) (getf room :height) (getf room :width)))


(defun concat-lists (seq1 seq2)
  (if (null seq1)
      seq2
      (cons (car seq1) (concat-lists (cdr seq1) seq2))))
