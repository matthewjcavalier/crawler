(in-package #:crawler)

(defun is-valid-room (room rooms-list settings)
  ;; makes a check to ensure that the input room is not
  ;; out of bounds and does not overlap other rooms
  ;;  (if (room-is-not-out-of-bounds room settings) (debug-print-dun (concat-lists rooms-list (list room)) settings))
  (and (room-is-not-out-of-bounds room settings)
       (room-does-not-overlap-other-rooms room rooms-list)))

(defun gen-room (max-row max-col min-height min-width)
  ;; randomly generate a room based on startup settings
  (let ((height (+ min-height (get-rand-int 10)))
	(width (+ min-width (get-rand-int 10))))
    (list :height height
	  :width width
	  :row (get-rand-int max-row)
	  :col (get-rand-int max-col))))

(defun generate-all-rooms (settings)
  (let ((not-done T)
	(new-room NIL)
	(rooms-list NIL)
	(min-rooms (getf settings :min-num-rooms))
	(min-room-height (getf settings :min-room-height))
	(min-room-width (getf settings :min-room-width))
	(max-width (- (getf settings :max-col) 1))
	(max-height (- (getf settings :max-row) 1))
	(attempts-made 0)
	(max-attempts 1000))
    (loop while not-done
       do (setq rooms-list
		(progn (setq new-room (gen-room max-height max-width min-room-height min-room-width))
		       (if (is-valid-room new-room rooms-list settings) (progn
									  (push new-room rooms-list)))
		       (setq attempts-made (+ 1 attempts-made))
		       (if (and (>= (list-length rooms-list) min-rooms) (> attempts-made max-attempts))
			   (progn
			     (print rooms-list)
			     (format t "~%")
			     (setq not-done NIL))
			   (if (> attempts-made max-attempts)
			       (progn (format T "Attempts Made: ~a~%" attempts-made) (format T "~%~%Rooms list length: ~a~%" (list-length rooms-list)) (debug-print-dun rooms-list settings) (setq attempts-made 0)
				      (setq rooms-list NIL))))
		       rooms-list)))
    rooms-list))

(defun debug-print-dun (rooms settings)
  (progn
    (describe-rooms rooms)
    (print-dun-debug (add-rooms-to-dun rooms (make-border (gen-grid (getf settings :max-row) (getf settings :max-col) (getf settings :max-stone-hardness)))))))

(defun room-is-not-out-of-bounds (room settings)
  (and (room-not-out-hor room settings)
       (room-not-out-vert room settings)))

(defun room-not-out-hor (room settings)
  (and (> (getf room :col) 0)
       (< (+ (getf room :width) (getf room :col)) (-  (getf settings :max-col) 1))))

(defun room-not-out-vert (room settings)
  (and (> (getf room :row) 0)
       (< (+ (getf room :height) (getf room :row)) (- (getf settings :max-row) 1))))

(defun room-does-not-overlap-other-rooms (room rooms-list)
  (let ((no-overlap T))
    (for:for ((comp-room over rooms-list))
      (setq no-overlap (and no-overlap (rooms-dont-overlap room comp-room))))
    no-overlap))

(defun rooms-dont-overlap (r1 r2)
  (and
   (rooms-dont-overlap-pass r1 r2)
   (rooms-dont-overlap-pass r2 r1)
   (rooms-dont-form-plus-symbol-pass r1 r2)
   (rooms-dont-form-plus-symbol-pass r2 r1))
  )

(defun rooms-dont-form-plus-symbol-pass (r1 r2)
  (let ((r1_corners (room-to-coords r1))
	(r2_corners (room-to-coords r2))
	(lines-cross NIL))
    (let ((vert-line (gen-coords-along-line (getf r1_corners :top-left) (getf r1_corners :bottom-left)))
	  (horiz-line (gen-coords-along-line (getf r2_corners :top-left) (getf r2_corners :top-right))))
      (for:for ((point over vert-line))
	(setq lines-cross (or lines-cross (member point horiz-line :test #'are-coords-equal)))))
    (not lines-cross)))

(defun rooms-dont-overlap-pass (r1 r2)
  (let ((with-buffer (room-to-coords (apply-buffer-to-room r1)))
	(no-overlap-found T))
    (for:for ((point over (plist-to-list (room-to-coords r2))))
      (setq no-overlap-found (and no-overlap-found (not (is-point-in-square point with-buffer)))))
    no-overlap-found))

(defun is-point-in-square (point square-coords)
  (and (>= (row point) (row (getf square-coords :top-left)))
       (<= (row point) (row (getf square-coords :bottom-left)))
       (>= (col point) (col (getf square-coords :top-left)))
       (<= (col point) (col (getf square-coords :top-right)))))

(defun apply-buffer-to-room (room)
  (list :row (- (getf room :row) 1)
	:col (- (getf room :col) 1)
	:height (+ (getf room :height) 2)
	:width (+ (getf room :width) 2)))

