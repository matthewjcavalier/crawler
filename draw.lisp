(in-package #:crawler)

(defun print-harndess-map (game-map)
  (dolist (row game-map)
    (progn
      (dolist (cell row)
	(format t "~a" (if (eq 'border (tile-type cell))
			   *frame-symbol*
			   (mod (hardness cell) 10))))
      (format t "~%"))))

(defun print-dun (game-map)
  (dolist (row game-map)
    (progn
      (dolist (cell row)
	(format t "~a" (cond ((eq 'border (tile-type cell)) *frame-symbol*)
			     ((eq 'room (tile-type cell)) *room-symbol*)
			     ((eq 'hall (tile-type cell)) *hall-symbol*)
			     (T *void-symbol*))))
      (format t "~%"))))

(defun print-dun-debug (game-map)
  (progn
    (let ((the-line " ") (the-top-line " "))
      (dotimes (i (list-length (nth 0 game-map)))
	(progn
	  (setq the-top-line (concatenate 'string the-top-line (if (= 0 (mod i 10))
								   (write-to-string (/ i 10))
								   " ")))
	  (setq the-line (concatenate 'string the-line (write-to-string (mod i 10))))))
      (format t "  ~a~%" the-top-line)
      (format t "  ~a~%" the-line)
      (let ((counter 0))
	(dolist (row game-map)
	  (progn
	    (format t "~a " (if (> 10 counter) (concatenate 'string " " (write-to-string counter)) (write-to-string counter)))
	    (setq counter (+ 1 counter))
	    (dolist (cell row)
	      (format t "~a" (cond ((eq 'border (tile-type cell)) *frame-symbol*)
				   ((eq 'room (tile-type cell)) *room-symbol*)
				   ((eq 'hall (tile-type cell)) *hall-symbol*)
				   (T *void-symbol*))))
	    (format t "~%")))
	(format t "  ~a~%" the-line)
	(format t "  ~a~%" the-top-line)))))
