(in-package #:crawler)

(defparameter *frame-symbol* "█")
(defparameter *void-symbol* "█")
(defparameter *room-symbol* ".")
(defparameter *hall-symbol* " ")

(defun reset-seeded-gen () (setq *seeded-gen* (random-state:make-generator :mersenne-twister-32 123)))

(defparameter *seeded-gen* NIL)
(reset-seeded-gen)

(defun check-nums ()
  (loop repeat 10
     collect (random-state:random-int *seeded-gen* 0 10)))

(defun get-rand-int (max &optional (min 0))
  ;; generates a random int between max and min, if no min is given then it is assumed that min is zero
  ;; Note: will be inclusive of upper and lower bounds
  (progn
    (random-state:random-int *seeded-gen* min max)))
